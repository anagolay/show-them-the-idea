#!/usr/bin/env bash

docker-compose kill
docker-compose rm --force

sudo rm -rf data/idiyanale_data
sudo rm -rf data/mongodb_data

docker-compose up -d

# give ipfs 5 secs to start
sleep 5

docker-compose exec ipfs ipfs bootstrap rm all &&
    docker-compose stop ipfs &&
    docker-compose up -d ipfs

sleep 5
# add our ipfs node
docker-compose exec ipfs ipfs swarm connect /ip4/159.69.27.167/udp/4001/quic/p2p/12D3KooWGc7qCqwQvx9r96hwtmVhJSiXKK1qMFunXP3KiccJv64w
