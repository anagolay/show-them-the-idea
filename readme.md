# Pin artifacts

Pin the cids to the local ipfs. Only to be done once when the pod is created.

```sh
# get into the container
docker-compose exec ipfs sh

# add the swarm addr again
ipfs swarm connect /ip4/159.69.27.167/udp/4001/quic/p2p/12D3KooWGc7qCqwQvx9r96hwtmVhJSiXKK1qMFunXP3KiccJv64w

# pin the stuff
ipfs pin add --progress

# while this is on copy the content of the artifacts.txt and paste then hit CTRL-D and

# wait
```

# Migration

In order to populate the local chain with operations from the testnet; this needs to be executed when the chain start new or after `./reset-env.sh`

```
anagolay migrate operations --to=wss://9944-anagolay-showthemtheide-17bz11w9rok-eu77.gitpod.io
```

# Anagolay app

Access the user interface  (https://app.anagolay.network) and configure the connection toward this pod wss